# galleria-spip



## Usage
> npm --version

> npm install

## Developing

CF : [Crayon plugin](https://zone.spip.org/trac/spip-zone/browser/_plugins_/crayons/trunk)

### Tools

Created with [Nodeclipse](https://github.com/Nodeclipse/nodeclipse-1)
 ([Eclipse Marketplace](http://marketplace.eclipse.org/content/nodeclipse), [site](http://www.nodeclipse.org))   

Nodeclipse is free open-source project that grows with your contributions.



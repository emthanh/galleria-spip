/**
* http://usejsdoc.org/
*/
module.exports = function (grunt) {

  grunt.task.registerTask('checkversion', 'check galleria version .', function() {
    var galleriapkg = grunt.file.readJSON('root/package.json')
    var spippacket = grunt.file.read('repo/paquet.xml')
    grunt.log.writeln('paquet.xml :\n' + spippacket);

    const xml2js = require('xml2js');
    const parser = new xml2js.Parser({ attrkey: "ATTR" });
    
    var galleria_plugin_ver = null
    parser.parseString(spippacket, (error, result) => {
        if(error === null) {
            galleria_plugin_ver = result.paquet.procure[0].ATTR.version
        }
    });

    grunt.log.writeln('current galleria version :' + galleriapkg.version);
    grunt.log.writeln("plugin galleria version :"+galleria_plugin_ver + "\nis new version " + (galleria_plugin_ver!==galleriapkg.version));
    if(galleria_plugin_ver===galleriapkg.version){
      grunt.log.writeln('no new version ' + galleriapkg.version + ', last version ' + galleria_plugin_ver)
      grunt.task.clearQueue()
    }
  });

  grunt.task.registerTask('noticeversion', 'notice galleria version .', function() {
    var galleriapkg = grunt.file.readJSON('root/package.json')
    grunt.fail.warn('new version '+galleriapkg.version)
  });


  grunt.initConfig({
    gitclone: {
      plugin: {
        options: {
          repository: 'https://git.spip.net/spip-contrib-extensions/galleria.git',
          branch: 'master',
          directory: 'repo'
        }
      },
      galleria: {
        options: {
          repository: 'https://github.com/GalleriaJS/galleria.git',
          branch: 'master',
          directory: 'root'
        }
      }

    },

    copy: {
      main: {
        files: [
          { expand: true, cwd: 'repo/', src: ['**', '!galleria'], dest: 'dist' },
          { expand: true, cwd: 'root/dist/', src: ['**'], dest: 'repo/galleria' }
        ]
      }
    },

    'string-replace': {
      dist: {
        files: [
          { src: ['repo/paquet.xml'], dest: 'repo/paquet.xml' }
        ],
        options: {
          replacements: [
            // place files inline example
            {
              pattern: /(.*version(="|>))([0-9]+.[0-9]+.)([0-9]+)(.*)/,
              replacement: function (match, p1, p2, p3, p4, p5) {
                var spip_plugin = grunt.file.readJSON('spip-plugin.json');
                var version = spip_plugin.plugin.version;
                grunt.log.writeln("change from version " + p3 + p4 + " to " + version);
                // give up p2
                return (p1 + version + p5);
              }
            },
            {
              pattern: /(.*version(="|>))([0-9]+.[0-9]+.)([0-9]+)(.*)\/>/,
              replacement: function (match, p1, p2, p3, p4, p5) {
                var galleriapkg = grunt.file.readJSON('root/package.json')
                var version = galleriapkg.version;
                grunt.log.writeln("change from version " + p3 + p4 + " to " + version);
                // give up p2
                return (p1 + version + p5 + ' />');
              }
            }
          ]
        }
      }
    },
    clean: {
      dist: {
        src: ["dist"],
        options: {
          force: false,
          'no-write': false
        }
      },
      repo: {
        src: ["repo"],
        options: {
          force: true,
          'no-write': false
        }
      },
      'repo-galleria': {
        src: ["repo/galleria"],
        options: {
          force: true,
          'no-write': false
        }
      },
      root: {
        src: ["root"],
        options: {
          force: true,
          'no-write': false
        }
      }
    },

    gitadd: {
      repo: {
        options: {
          force: true
        },
        files: {
          repo: ['repo/**/*']
        }
      }

    }

  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-string-replace');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-git');


  grunt.registerTask('build', ['gitclone', 'checkversion' , 'clean:repo-galleria', 'copy', 'string-replace', 'noticeversion']);
  grunt.registerTask('default', ['build']);

};